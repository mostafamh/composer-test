<?php
$host= 'localhost';
$port ='3306';
$database = 'fakename';
$username ='root';
$password = 'password'; 
$charset = 'utf8mb4';
$options = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
];
try {
    $conn = new PDO("mysql:host=$host;dbname=$database;charset=$charset", $username, $password,$options,);
} catch (PDOException $error) {
    var_dump($error->getMessage());
}
?>
